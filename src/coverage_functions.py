from epynet import Network
import numpy as np
import math as m
import operator
#import pandas as pd
import matplotlib.pyplot as plt

#import sys
#sys.path.insert(0, '../src/')
from network_grouping import *

# Calculate the diference of sensor pressure when there is a leak in each node of a i-group
def node_dif_pressures(inp, leak, leak_id, y, i):

    y_dif_sensor = []

    sensor = nodes_next_center(inp, y, i) # Take a sensor of group    
    node_list = [key for key in y.keys() if y[key] == i] # Take the nodes in this group
    n_nodes_group = len(node_list)
        
    for j in range(n_nodes_group): # node by node 

   #      Pressure at sensors
        y_simulated_sensor = simulation(inp, sensorIds=sensor, nodeIds=[str(node_list[j])])
        y_simulated_leak_sensor = simulation(inp, leaks = leak, sensorIds=sensor, nodeIds=[str(node_list[j])], leak_id = leak_id)

        y_dif_sensor.append(np.mean((y_simulated_sensor - y_simulated_leak_sensor))) # Mean at time

    node_value_group = [[node_list[indice], y_dif_sensor[indice]] for indice in range(n_nodes_group)]
    node_value_group = dict(node_value_group)
    node_value_group = sorted(node_value_group.items(), key=operator.itemgetter(1))  # Put the values in ascending order

    return node_value_group # node with its diference pressure value


# Separate the values in node_value according specifics range of numbers

def separate_degrade(node_value, values):
    group = []
    cont = 0
    node = 0
    i = 0
    while node < len(node_value) and i < len(values):
        if node_value[node][1] < values[i]: 
            cont = cont + 1
            node = node + 1   
        else:    
            group = group + cont*[i]
            i = i + 1

    n_residual = len(node_value) - len(group)   # Number of nodes that are without group

    if n_residual > 0:
        group = group + n_residual*[i + 1] 

    node_degrade = dict([[node_value[index][0], group[index]] for index in range(len(node_value))])

    return node_degrade  # node with its class
  
# Calculate the porcentage of coverage using a threshold
def porcentage(node_value, threshold):
    
    cont = 0
    for el in node_value:
        if el[1] < threshold:
            cont = cont + 1
    
    n = len(node_value)
    p = 100 - np.divide(100*cont, n)
    
    return p

# Draw the graph groups in degrade
def graph_degrade(inp, y, n_groups, node_degrade, values):
    net = inp_to_graph(inp)
    cmap = plt.cm.get_cmap('PuBu', len(values))

    for i_tone in range(len(values)):    
        node_list = [key for key in node_degrade.keys() if node_degrade[key] == i_tone] # List of all nodes in i_groups

        draw_networkx(net, with_labels=False, pos=net.graph['COORDINATES'], node_size=60, nodelist = node_list,
                    node_color = cmap(i_tone))

    # Center
    for i in range(n_groups):
        center = center_group(inp, y, i)
        plt.scatter(center[:,0], center[:,1], marker="8", linewidths = 20, color= 'black')


    fig_size = [0,0]
    fig_size[0] = 7
    fig_size[1] = 10
    plt.rcParams["figure.figsize"] = fig_size
    plt.title ("Leak flow in each node")
    fig_size
    #plt.savefig(str(inp)+ str(n_groups) + 'groups.png')
    plt.show()
    
# To save node_coverage

def save_dict(node_value):
    nodes = np.asarray(node_value.keys())
    values = np.asarray(node_value.values())

    nodes_groups = np.stack((nodes,values), axis = 1)
    n = len(nodes_groups)

    np.savetxt('node_value_' + str(n) + 'groups.csv', nodes_groups, fmt=("%s","%s"), delimiter=',')