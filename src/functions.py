from epynet import Network
import numpy as np

    
def binarization(sensitivity_matrix, residue, threshold_m, threshold_r):
   
    matriz_sen_bin = np.greater(sensitivity_matrix, threshold_m) # Binarize sensitiviy_matrix
    residue_bin = np.greater(residue, threshold_r)   # Binarize residue
    
    result = [np.array_equal(node, residue_bin) for node in matriz_sen_bin] # Compare the residue with each
                                                                            # column of sensitivity_matrix_bin
        
    leak_nodes = [i for i in range(len(result)) if result[i] is True]   # Select the indices with true results
    
    return result, leak_nodes 

def correlation(sensitivity_matrix, residue):

    residue_m = np.mean(residue, 0)    # Calculate the mean of residue for each sensor

    matrix_sen_m_aux = np.mean(sensitivity_matrix, 1) # Calculate the mean of sensitivity_matrix for each sensor
        
    matrix_sen_m = [sensitivity_matrix.shape[1]*[el] for el in matrix_sen_m_aux] # Replicate the means values for each sensor
    matrix_sen_m = np.asarray(matrix_sen_m)  

    t1 = residue - residue_m
    t1 = np.asarray(sensitivity_matrix.shape[0] * [t1])   
    
    t2 = sensitivity_matrix - matrix_sen_m
    
    t12 = np.sum(t1*t2,1)
    
    t3 = np.power((residue - residue_m),2)
    t3 = np.sqrt(np.sum(t3,0))
    t3 = np.asarray(sensitivity_matrix.shape[0] * [t3]).reshape(sensitivity_matrix.shape[0],sensitivity_matrix.shape[2])

    t4 = np.power((sensitivity_matrix - matrix_sen_m),2)
    t4 = np.sqrt(np.sum(t4,1))

    t34 = t3 * t4

    c = np.mean(t12/t34,1)
    c = c.tolist()
    leak_node = c.index(max(c)) # Select the node with maximum value of correlation

    return leak_node

def euclideanDistance(sensitivity_matrix, residue, emitter):

    t1 = np.asarray(sensitivity_matrix.shape[0] * [residue]) # Replicate the residue
    t2 = emitter * sensitivity_matrix

    d = np.power((t1 - t2), 2)
    d = np.sqrt(np.sum(d,1))
    d = np.mean(d,1)
    
    d = d.tolist()
    leak_node = d.index(min(d)) # The smallest distance will be a possible leak_node
    
    return leak_node

def angleBetweenVectors(sensitivity_matrix, residue):

    residue = np.asarray(sensitivity_matrix.shape[0]*[residue])

    t1 = np.sum(residue*sensitivity_matrix, 1) # Scalar product between residue each columns of sensors

    residue_norm = np.linalg.norm(residue)  
    sensitivity_matrix_norm = np.linalg.norm(sensitivity_matrix, axis = 1)

    t2 = residue_norm*sensitivity_matrix_norm  

    angle = np.arccos(t1/t2)
    angle = np.mean(angle, 1).tolist()  # Calculate the angle mean for each node
    leak_node = angle.index(min(angle)) # The smallest angle will be a possible leak_node

    return leak_node